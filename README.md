# vim-deadkeys

Deadkey map vim plugin to enable typing accented characters.

Modified from [Luke Smith's plugin](https://github.com/LukeSmithxyz/vimling).
